__author__ = 'phe'
__created__ = '5/12/15'

if __name__ == '__main__':
    pass
else:

    '''
    '''

    class PheXception(Exception):
        def __init__(self, arg):
            super(Exception, self).__init__(arg)

    class Typecheck(PheXception):
        def __init__(self, i):
            self.__type__ = type(i)

        def __setattr__(self, key, value):
            if (key.find('__type__') != 0) and (type(value) != self.__type__):
                raise PheXception("({0:s}) is not {1:s}".format(key, self.__type__.__name__))
            self.__dict__[key] = value